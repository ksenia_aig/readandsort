package com.company;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Main {

    List<Integer> numbers = new ArrayList<>();

    public List<Integer> reading() throws Exception {
        for (String line : Files.readAllLines(Paths.get("Number.txt"))) {
            for (String part : line.split(",")) {
                Integer i = Integer.valueOf(part);
                numbers.add(i);
            }
        }
        return numbers;
    }

    public void sorting() throws Exception {
        List<Integer> col = reading();
        Collections.sort(col);
        System.out.println(col);
    }

    public void reverseSorting() throws Exception {
        List<Integer> col = reading();
        Collections.sort(col);
        Collections.reverse(col);
        System.out.println(col);
    }

    public static void main(String[] args) throws Exception {
        {
            try {
                Main readAndSort = new Main();
                System.out.println("1 - Вывести по возрастанию;" + "\n" +
                        "2  - Вывести по убыванию;" + "\n" +
                        "3 - Выход."
                );
                Scanner in = new Scanner(System.in);
                int num = in.nextInt();
                if (num == 1) {
                    readAndSort.sorting();
                }
                if (num == 2) {
                    readAndSort.reverseSorting();
                }
                if (num == 3) {
                    System.exit(0);
                }
            } catch (Exception ex) {
                System.out.println("Введите 1,2 или 3!");
            }
        }
    }
}